$(document).ready( function() {
    Classes()
    trocarNomeBotao()
    trocarNome()
    trocarRedesSociais()
    placeHolder()
    TrocarTituloNews()
    FormasDePagamento()
});


function Classes () {
    $('header .col-xs-6').addClass('col-md-4');
    $('header .col-xs-3').addClass('col-md-4');
    $('header .col-xs-3').removeClass('col-xs-3').addClass('col-xs-12')
    $('header .col-xs-6').removeClass('col-xs-6').addClass('col-xs-12')
    $('header form button').removeClass('btn-default')
    $('main .col-xs-3').removeClass('col-xs-3').addClass('col-xs-12 col-sm-4 col-md-3')
    $('.newsletter .col-xs-6:first-child').removeClass('col-xs-6').addClass('col-xs-12 col-sm-4 col-md-4')
    $('.newsletter .col-xs-6').removeClass('col-xs-6').addClass('col-xs-12 col-sm-5 col-md-5')
    $('.insitucional .col-xs-3').removeClass('col-xs-3').addClass('col-xs-12 col-sm-3 col-md-3')
}

function placeHolder () {
    $('.newsletter form input').attr('placeholder', 'Digite seu e-mail');
    $('header form input').attr('placeholder', 'Digite o que você procura')
}

function TrocarTituloNews () {
    $('.newsletter .title').remove('.newsletter .title')
    $('.newsletter .col-sm-4').html("<div class='title'><i class='fa fa-envelope'></i> Receba nossas ofertas por e-mail</div>")
}

function trocarHeader () {
    $('header .container .col-md-4:first-child()').remove()
}

function trocarHeader2 () {
    $('header .container .col-md-4:first-child(2)').remove()
}

function trocarRedesSociais () {
        var social = $('.insitucional ul.list-inline').remove().insertAfter($('.newsletter .container .col-xs-12.col-sm-5'))
        .addClass('col-xs-12 col-md-3 col-sm-3')
}

function trocarNome () {
    $('footer .payment h3').text('Como Pagar');
}

function trocarNomeBotao () {
    $('.newsletter .glyphicon.glyphicon-chevron-right').removeClass('glyphicon glyphicon-chevron-right');
    $(".newsletter .btn.btn-default").text("ASSINAR");

}

function FormasDePagamento() {
    var pagseguro = "<li><img src='' alt='Forma de pagamento PagSeguro'></li>"
    $('.payment ul:last-child() li').remove();
    $('.payment ul:last-child()').html("<li><img src='https://cdn.shopify.com/s/files/1/3105/5634/files/PagSeguro-Uol-1_large.png?v=1518965154' height='80' width='150' alt='Forma de pagamento PagSeguro'></li>");
}